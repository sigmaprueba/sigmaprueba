﻿lista = null;
function setCiudad() {
    var keyDepartamento = $('#departamento').val();
    var cities = lista[keyDepartamento];
    $('#ciudad').children().remove();
    addOptions("ciudad", cities);
}
function getcolombia() {
    $.ajax({
        url: '/api/colombia',
        success: function (respuesta) {
            lista = JSON.parse(respuesta);
            var state = Object.keys(lista)
            addOptions("departamento", state);
            var keyDepartamento = $('#departamento').val();
            var cities = lista[keyDepartamento];
            addOptions("ciudad", cities);

        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });
    $('#nombre').focus();
}

function save() {

    state = $('#departamento').val();
    city = $('#ciudad').val();
    name = $('#nombre').val();
    email = $('#correo').val();
    url = '/home/Post'
    json = { Id: 0, departamento: state, ciudad: city, nombre: name, correo: email }
    
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        success: function (resultado) {
                   console.log(resultado)
                   $("#mensaje").text(resultado);
                   $("#miModal").modal("show");
               },
               error: function (err) {
                   console.log(err)
               }
           });
}

function addOptions(domElement, array) {
    var $mySelect = $('#' + domElement + '');
    //
    $.each(array, function (key, value) {
        var $option = $("<option/>", {
            value: value,
            text: value
        });
        $mySelect.append($option);
    });
}

function validarEmail() {
    email = $('#correo').val();
    regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (email == 'undefined') {
        alert("Debe escribir un Correo valido")
        $('#correo').focus();
    }
    if(!regex.test(email)){
        alert("correo No valido")
        $('#correo').focus();
    } 
}

function validaNombre() {
    if (($('#nombre').val() == 'undefined') || (($('#nombre').val()).trim() == '')) {
        alert("Debe escribir su nombre completo")
        $('#nombre').focus();
    }
}