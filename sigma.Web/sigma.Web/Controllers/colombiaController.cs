﻿using sigma.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace sigma.Web.Controllers
{
    public class colombiaController : ApiController
    {
        // GET: api/colombia
        public async Task<string> Get()
        {
            return await Consultar();
        }


        public async Task<string> Consultar()
        {
            try
            {
                HttpClient client;
                client = new HttpClient();
                client.MaxResponseContentBufferSize = 256000;
                string url = "https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json";
                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();

                    return content;
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
