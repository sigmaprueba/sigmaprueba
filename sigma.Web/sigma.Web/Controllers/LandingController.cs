﻿using sigma.Aplication.Landing;
using sigma.Aplication.Landing.Dto;
using sigma.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace sigma.Web.Controllers
{
    public class LandingController : ApiController
    {
        private readonly ILandingAppService _landingAppService;
        public LandingController(ILandingAppService landingAppService)
        {
            _landingAppService = landingAppService;
        }
        // GET: api/Landing
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Landing/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Landing
        public async Task<string> Post([FromBody] LadingInputDto input)
        {
            string respuesta = "";
            bool resp = await _landingAppService.guardar(input);
            if(resp) respuesta = "Tu información ha sido recibida satisfactoriamente";
            return respuesta;
        }

        // PUT: api/Landing/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Landing/5
        public void Delete(int id)
        {
        }
    }
}
