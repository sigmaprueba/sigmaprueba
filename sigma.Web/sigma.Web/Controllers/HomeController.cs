﻿using Newtonsoft.Json;
using sigma.Aplication.Landing;
using sigma.Aplication.Landing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;

namespace sigma.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILandingAppService _landingAppService;
        public HomeController(ILandingAppService landingAppService)
        {
            _landingAppService = landingAppService;
        }
        public ActionResult Index()
        {
            return View();
        }

        

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public async Task<string> Post([FromBody] LadingInputDto input)
        {
            string respuesta = "";
            bool resp = await _landingAppService.guardar(input);
            if (resp) respuesta = "Tu información ha sido recibida satisfactoriamente";
            return respuesta;
        }
    }
}