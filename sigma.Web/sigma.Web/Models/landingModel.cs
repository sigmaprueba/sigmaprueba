﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sigma.Web.Models
{
    public class landingModel
    {
        public string state { get; set; }
        public string city { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }
}