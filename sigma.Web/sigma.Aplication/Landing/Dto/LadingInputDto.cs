﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sigma.Aplication.Landing.Dto
{
    public class LadingInputDto
    {
        public int Id { get; set; }
        public string departamento { get; set; }
        public string ciudad { get; set; }
        public string nombre { get; set; }
        public string correo { get; set; }
    }
}
