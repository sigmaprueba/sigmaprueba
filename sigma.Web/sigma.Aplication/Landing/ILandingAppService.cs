﻿using sigma.Aplication.Landing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sigma.Aplication.Landing
{
    public interface ILandingAppService
    {
        Task<Boolean> guardar(LadingInputDto input);
    }
}
