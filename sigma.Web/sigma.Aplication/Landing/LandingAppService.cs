﻿using sigma.Aplication.Landing.Dto;
using sigma.Core.Entidades;
using sigma.EntityFramework.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sigma.Aplication.Landing
{
    public class LandingAppService : ILandingAppService
    {
        private readonly sigmaDbContext _dbContext;

        public LandingAppService()
        {
            _dbContext = new sigmaDbContext();
        }
        public async Task<bool> guardar(LadingInputDto input)
        {
            try
            {
                bool resp = false;
                LandingEntity entity = new LandingEntity();
                entity.nombre = input.nombre;
                entity.correo = input.correo;
                entity.departamento = input.departamento;
                entity.ciudad = input.ciudad;
                _dbContext.landingEntities.Add(entity);
                var id=await _dbContext.SaveChangesAsync();
                if (id > 0) resp = true;
                return resp;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
