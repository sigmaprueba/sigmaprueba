﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sigma.Core.Entidades
{
    public class LandingEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(30)]
        public string departamento { get; set; }
        [Required]
        [MaxLength(50)]
        public string ciudad { get; set; }
        [Required]
        [MaxLength(50)]
        public string nombre { get; set; }
        [Required]
        [MaxLength(30)]
        public string correo { get; set; }

    }
}
