﻿using MySql.Data.EntityFramework;
using sigma.Core.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sigma.EntityFramework.EntityFramework
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class sigmaDbContext : DbContext
    {
        public sigmaDbContext(): base("Default") { }
        public DbSet<LandingEntity> landingEntities { get; set; }
    }
}
