﻿namespace sigma.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracionInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LandingEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        departamento = c.String(nullable: false, maxLength: 30, storeType: "nvarchar"),
                        ciudad = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        nombre = c.String(nullable: false, maxLength: 50, storeType: "nvarchar"),
                        correo = c.String(nullable: false, maxLength: 30, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LandingEntities");
        }
    }
}
